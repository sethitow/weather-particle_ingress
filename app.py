import logging

from flask import Flask, request, g
from google.cloud import firestore

app = Flask(__name__)
logging.basicConfig(level=logging.DEBUG)

def get_db():
    if not hasattr(g, 'db'):
        g.db = firestore.Client()
    return g.db

@app.route("/particle-ingress")
def hello():
    data = request.args.to_dict()
    app.logger.info(data)
    get_db().collection(u'stream').add(data)
    return "OK"

if __name__ == "__main__":
    app.run(debug=True)